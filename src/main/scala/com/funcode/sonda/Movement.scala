package com.funcode.sonda

import scala.annotation.tailrec
import scala.beans.BeanProperty


sealed trait Command

case object MOVE extends Command
case object TURN_LEFT extends Command
case object TURN_RIGHT extends Command

sealed trait Direction

case object NORTH extends Direction
case object EAST extends Direction
case object SOUTH extends Direction
case object WEST extends Direction

case class OrientedPosition(@BeanProperty direction: Direction, @BeanProperty position: Position)

case class Position(@BeanProperty x : Int, @BeanProperty y: Int)


object Movement {

  def move(op: OrientedPosition, command: Command) : OrientedPosition = command match {
    case TURN_LEFT => op.copy(direction = turnLeft(op.direction))
    case TURN_RIGHT => op.copy(direction = turnRight(op.direction))
    case MOVE =>  op.copy(position = moveForward(op.position, op.direction))
  }

  private def moveForward(position: Position, direction: Direction) : Position = direction match {
    case NORTH => position.copy(y=position.y+1)
    case SOUTH => position.copy(y=position.y-1)
    case EAST => position.copy(x=position.x+1)
    case WEST => position.copy(x=position.x-1)
  }

  private def turnRight(origin: Direction) : Direction =  {
    val directionsClockwise =  Seq(NORTH, EAST, SOUTH, WEST)

    turn(origin, directionsClockwise)
  }

  private def turnLeft(origin: Direction) : Direction =  {
    val directionsCounterClockwise =  Seq(NORTH, WEST, SOUTH, EAST)

    turn(origin, directionsCounterClockwise)
  }

  private def turn(origin: Direction, directions: Seq[Direction]) : Direction =  {
    @tailrec
    def circularNextRec(list: Seq[Direction], origin: Direction, firstElement: Direction) : Direction = list match {
      case _::Nil => firstElement
      case head::tail => if (head.equals(origin)) tail.head else circularNextRec(tail, origin, firstElement)
    }

    circularNextRec(directions, origin, directions.head)
  }
}
