package com.funcode.sonda


import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.http.{HttpStatus, ResponseEntity}
import org.springframework.web.bind.annotation.RequestMethod.{GET, POST}
import org.springframework.web.bind.annotation.{PathVariable, RequestBody, RequestMapping, RestController}


import scalaz._

@RestController
@RequestMapping(value = Array("/probes"), produces=Array(APPLICATION_JSON_VALUE), consumes=Array(APPLICATION_JSON_VALUE))
class ProbeResource {

  @RequestMapping(method = Array(POST), value = Array("/{probeId}/move"))
  def move(@RequestBody commands: Commands, @PathVariable probeId: String): ResponseEntity[Any] = {
    val parsedCommands = commands.movements.toCharArray.map(CommandParser.parse)

    val position = new OrientedPosition(NORTH, new Position(x=3, y=2)) //buscar do banco baseado no probeId
    val zero : Valid[OrientedPosition] = \/.right(position)

    val result = parsedCommands.foldLeft(zero)((pos, cmd) => (pos, cmd) match {
      case (\/-(pos), \/-(c)) => \/.right(Movement.move(pos, c)) // tem q haver um jeito mais bonito de extrair isso aqui...
      case _ => \/.left(NonEmptyList("Invalid Movement"))
    })

    result match {
      case \/-(orientedPosition) => new ResponseEntity[Any](orientedPosition.position, HttpStatus.OK)
      case -\/(message) => new ResponseEntity[Any](message.toString, HttpStatus.BAD_REQUEST)
    }
  }
}


