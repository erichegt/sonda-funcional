package com.funcode

import scalaz.{NonEmptyList, \/}

package object sonda {
  type Valid[A] = NonEmptyList[String] \/ A
}
