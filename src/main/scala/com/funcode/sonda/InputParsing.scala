package com.funcode.sonda

import com.fasterxml.jackson.annotation.{JsonCreator, JsonProperty}

import scala.beans.BeanProperty
import scalaz._


case class Commands @JsonCreator()(@JsonProperty("movements") @BeanProperty movements: String)



trait CommandParser {

  def parse(char: Char) : Valid[Command]

  def extract(commands: Commands) : Valid[Array[Char]]

}

object CommandParser extends  CommandParser {

  def parse(char: Char) = char match {
    case 'M' => \/-(MOVE)
    case 'L' => \/-(TURN_LEFT)
    case 'R' => \/-(TURN_RIGHT)
    case c => -\/(NonEmptyList(s"Invalid command: $c"))
  }

  def extract(commands: Commands) = commands.movements match {
    case "" => -\/(NonEmptyList("Empty commands for probe movement"))
    case moves => \/-(moves.toCharArray)
  }
}
